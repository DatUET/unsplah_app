import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:unsplah_app/domain/entities/photo.dart';
import 'package:unsplah_app/generated/assets.gen.dart';
import 'package:unsplah_app/widget/image_loading.dart';

class PhotoWidget extends StatelessWidget {
  final Photo photo;
  final VoidCallback? onTap;

  const PhotoWidget({super.key, required this.photo, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12.r),
        child: CachedNetworkImage(
          imageUrl: photo.urls?.regular ?? '',
          fit: BoxFit.cover,
          placeholder: (context, url) => const ImageLoading(),
          errorWidget: (context, state, err) => Assets.images.logo.svg(),
        ),
      ),
    );
  }
}
