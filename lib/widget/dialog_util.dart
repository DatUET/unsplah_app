import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:unsplah_app/core/extension/string_extension.dart';
import 'package:unsplah_app/l10n/l10n.dart';

class DialogUtil {
  static bool isShowingDialog = false;
  static String defaultTitle = 'Notification';
  static String defaultTitleError = 'Error';
  static double radiusMaterialDialog = 8.0;
  static TextStyle? confirmStyle;
  static TextStyle? cancelStyle;

  static Future<T?> confirm<T>(
    BuildContext context, {
    String? title,
    String content = '',
    TextStyle? titleStyle,
    TextStyle? contentStyle,
    String? submitText,
    String? cancelText,
    Function? onTapSubmit,
    Function? onTapCancel,
  }) {
    isShowingDialog = true;
    if (Platform.isIOS) {
      return showCupertinoDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => CupertinoAlertDialog(
                title: Text(
                  title ?? defaultTitle,
                  style: titleStyle,
                ),
                content: Text(
                  content,
                  style: contentStyle,
                ),
                actions: <Widget>[
                  CupertinoDialogAction(
                      child: Text(cancelText.isNullOrEmpty ? context.l10n.cancel : cancelText!),
                      onPressed: () {
                        Navigator.of(context).pop();
                        isShowingDialog = false;
                        if (onTapCancel != null) {
                          onTapCancel();
                        }
                      }),
                  CupertinoDialogAction(
                    child: Text(submitText.isNullOrEmpty ? context.l10n.ok : submitText!),
                    onPressed: () {
                      Navigator.of(context).pop();
                      isShowingDialog = false;
                      if (onTapSubmit != null) {
                        onTapSubmit();
                      }
                    },
                  )
                ],
              ));
    }
    return showDialog<T>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(child: Text(title ?? defaultTitle)),
          elevation: 8,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(radiusMaterialDialog)),
          content: Text(content),
          actions: <Widget>[
            TextButton(
                child: Text(cancelText.isNullOrEmpty ? context.l10n.cancel : cancelText!, style: cancelStyle),
                onPressed: () {
                  Navigator.of(context).pop();
                  isShowingDialog = false;
                  if (onTapCancel != null) {
                    onTapCancel();
                  }
                }),
            TextButton(
              child: Text(submitText.isNullOrEmpty ? context.l10n.ok : submitText!, style: confirmStyle,),
              onPressed: () {
                Navigator.of(context).pop();
                isShowingDialog = false;
                if (onTapSubmit != null) {
                  onTapSubmit();
                }
              },
            )
          ],
        );
      },
    );
  }

  static Future<T?> alert<T>(BuildContext context,
      {String? title,
      String content = '',
      TextStyle? titleStyle,
      TextStyle? contentStyle,
      String? submit,
      Function? onSubmit}) {
    isShowingDialog = true;
    if (Platform.isIOS) {
      return showCupertinoDialog(
        context: context,
        useRootNavigator: false,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text(
              title ?? defaultTitle,
              style: titleStyle,
            ),
            content: Text(
              content,
              style: contentStyle,
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                child: Text(submit.isNullOrEmpty ? context.l10n.ok : submit!),
                onPressed: () {
                  isShowingDialog = false;
                  Navigator.of(context).pop();
                  if (onSubmit != null) {
                    onSubmit();
                  }
                },
              )
            ],
          );
        },
      );
    }
    return showDialog<T>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          elevation: 8,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(radiusMaterialDialog)),
          title: Center(child: Text(title ?? defaultTitle)),
          titleTextStyle: titleStyle,
          alignment: Alignment.center,
          content: Text(
            content,
            style: contentStyle,
          ),
          actions: <Widget>[
            TextButton(
              child: Text(submit.isNullOrEmpty ? context.l10n.ok : submit!, style: confirmStyle,),
              onPressed: () {
                isShowingDialog = false;
                Navigator.of(context).pop();
                if (onSubmit != null) {
                  onSubmit();
                }
              },
            )
          ],
        );
      },
    );
  }

  static Future<T?> error<T>(BuildContext context,
      {String? title,
      String content = '',
      TextStyle? titleStyle,
      TextStyle? contentStyle,
      bool isShowRetry = false,
      String? retryText,
      Function? onTapRetry,
      String closeText = '',
      Function? onTapClose}) {
    isShowingDialog = true;
    if (Platform.isIOS) {
      return showCupertinoDialog(
        context: context,
        useRootNavigator: false,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text(
              title ?? defaultTitleError,
              style: titleStyle,
            ),
            content: Text(
              content,
              style: contentStyle,
            ),
            actions: <Widget>[
              if (isShowRetry)
                CupertinoDialogAction(
                  child: Text(retryText.isNullOrEmpty ? context.l10n.retry : retryText!),
                  onPressed: () {
                    isShowingDialog = false;
                    Navigator.of(context).pop();
                    if (onTapRetry != null) {
                      onTapRetry();
                    }
                  },
                ),
              CupertinoDialogAction(
                child: Text(closeText),
                onPressed: () {
                  isShowingDialog = false;
                  Navigator.of(context).pop();
                  if (onTapClose != null) {
                    onTapClose();
                  }
                },
              )
            ],
          );
        },
      );
    }
    return showDialog<T>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          elevation: 8,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(radiusMaterialDialog)),
          title: Center(child: Text(title ?? defaultTitleError)),
          titleTextStyle: titleStyle,
          alignment: Alignment.center,
          content: Text(
            content,
            style: contentStyle,
          ),
          actions: <Widget>[
            TextButton(
              child: Text(closeText, style: cancelStyle,),
              onPressed: () {
                isShowingDialog = false;
                Navigator.of(context).pop();
                if (onTapClose != null) {
                  onTapClose();
                }
              },
            ),
            if (isShowRetry)
              TextButton(
                child: Text(retryText.isNullOrEmpty ? context.l10n.retry : retryText!, style: confirmStyle,),
                onPressed: () {
                  isShowingDialog = false;
                  Navigator.of(context).pop();
                  if (onTapRetry != null) {
                    onTapRetry();
                  }
                },
              )
          ],
        );
      },
    );
  }

  static Future<T?> confirmSheet<T>(
    BuildContext context, {
    String? title,
    TextStyle? titleStyle,
    String? content,
    String? cancelText,
    String? submitText,
    Color? submitColor,
    Color? cancelColor,
  }) {
    return showCupertinoModalPopup(
        context: context,
        useRootNavigator: false,
        builder: (popupContext) {
          return CupertinoActionSheet(
            title: Text(
              title ?? defaultTitle,
              style: titleStyle,
            ),
            message: (content != null && content.isNotEmpty)
                ? Container(
                    alignment: Alignment.center,
                    child: Text(content, style: Theme.of(context).textTheme.caption),
                  )
                : null,
            actions: [
              CupertinoActionSheetAction(
                onPressed: () {
                  Navigator.of(popupContext).pop(true);
                },
                child: Text(
                  submitText ?? "Xác nhận",
                  style: TextStyle(
                    color: submitColor ?? const Color(0XFF007AFF),
                    fontWeight: FontWeight.w400,
                  ),
                ),
              )
            ],
            cancelButton: CupertinoActionSheetAction(
              onPressed: () {
                Navigator.of(popupContext).pop();
              },
              child: Text(
                cancelText ?? "Hủy",
                style: TextStyle(
                  color: cancelColor ?? const Color(0XFF007AFF),
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          );
        });
  }
}
