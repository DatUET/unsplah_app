import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:unsplah_app/core/extension/string_extension.dart';

class CommonTextField extends StatelessWidget {
  static CommonTextFieldStyle commonTextFieldStyle = const CommonTextFieldStyle();

  final String? title;
  final String? note;
  final TextEditingController? controller;
  final String? hint;
  final String? error;
  final Function(String)? onChange;
  final Function()? onTap;
  final Function(String)? onSubmit;
  final Function()? onEditingComplete;
  final int maxLine;
  final int? minLine;
  final int? maxLength;
  final TextInputAction? textInputAction;
  final TextAlign textAlign;
  final bool? enabled;
  final bool autofocus;
  final TextInputType? keyboardType;
  final bool readOnly;
  final bool obscureText;
  final FocusNode? focusNode;
  final Color? cursorColor;
  final List<TextInputFormatter>? inputFormatters;
  final bool? showCursor;
  final TextStyle? labelStyle;
  final TextStyle? noteStyle;
  final TextStyle? hintStyle;
  final TextStyle? errorStyle;
  final Color? fillColor;
  final bool? filled;
  final Widget? suffix;
  final BoxConstraints? suffixConstraints;
  final Widget? prefix;
  final BoxConstraints? prefixBoxConstrains;
  final int? hintMaxLine;
  final bool? alignLabelWithHint;
  final EdgeInsetsGeometry? contentPadding;
  final double? radius;
  final Color? borderColor;
  final Color? disableBorderColor;
  final Color? enableBorderColor;
  final Color? errorBorderColor;
  final Color? focusedBorderColor;
  final Color? focusedErrorBorderColor;
  final bool isUnderline;

  const CommonTextField({
    Key? key,
    this.title,
    this.note,
    this.controller,
    this.hint,
    this.error,
    this.onChange,
    this.onTap,
    this.onSubmit,
    this.onEditingComplete,
    this.maxLine = 1,
    this.minLine,
    this.maxLength,
    this.textInputAction,
    this.textAlign = TextAlign.start,
    this.enabled,
    this.autofocus = false,
    this.keyboardType,
    this.readOnly = false,
    this.obscureText = false,
    this.focusNode,
    this.cursorColor,
    this.inputFormatters,
    this.showCursor,
    this.labelStyle,
    this.noteStyle,
    this.hintStyle,
    this.errorStyle,
    this.fillColor,
    this.filled,
    this.suffix,
    this.suffixConstraints,
    this.prefix,
    this.prefixBoxConstrains,
    this.hintMaxLine,
    this.alignLabelWithHint,
    this.contentPadding,
    this.radius,
    this.borderColor,
    this.disableBorderColor,
    this.enableBorderColor,
    this.errorBorderColor,
    this.focusedBorderColor,
    this.focusedErrorBorderColor,
    this.isUnderline = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      onTap: onTap,
      onChanged: onChange,
      onSubmitted: onSubmit,
      onEditingComplete: onEditingComplete,
      maxLines: maxLine,
      maxLength: maxLength,
      minLines: minLine,
      textInputAction: textInputAction,
      textAlign: textAlign,
      enabled: enabled,
      autofocus: autofocus,
      keyboardType: keyboardType,
      readOnly: readOnly,
      obscureText: obscureText,
      focusNode: focusNode,
      cursorColor: cursorColor,
      inputFormatters: inputFormatters,
      showCursor: showCursor,
      style: labelStyle ?? commonTextFieldStyle.labelStyle,
      decoration: InputDecoration(
        hintText: hint,
        hintStyle: hintStyle ?? commonTextFieldStyle.hintStyle,
        fillColor: fillColor,
        filled: filled,
        suffixIcon: suffix,
        suffixIconConstraints: suffixConstraints,
        prefixIcon: prefix,
        prefixIconConstraints: prefixBoxConstrains,
        hintMaxLines: hintMaxLine,
        alignLabelWithHint: alignLabelWithHint,
        isDense: true,
        errorText: error.isNotNullOrEmpty ? error : null,
        errorStyle: errorStyle ?? commonTextFieldStyle.errorStyle,
        contentPadding: contentPadding ?? commonTextFieldStyle.contentPadding,
        errorMaxLines: 2,
        border: isUnderline
            ? UnderlineInputBorder(
                borderSide: BorderSide(
                    color: borderColor ?? commonTextFieldStyle.borderColor!, width: 1.0),
              )
            : OutlineInputBorder(
                borderRadius: BorderRadius.circular(radius ?? commonTextFieldStyle.borderRadius),
                borderSide: BorderSide(
                    color: borderColor ?? commonTextFieldStyle.borderColor!, width: 1.0),
              ),
        disabledBorder: isUnderline
            ? UnderlineInputBorder(
                borderSide: BorderSide(
                    color: disableBorderColor ?? commonTextFieldStyle.disableBorderColor!,
                    width: 1.0),
              )
            : OutlineInputBorder(
                borderRadius: BorderRadius.circular(radius ?? commonTextFieldStyle.borderRadius),
                borderSide: BorderSide(
                    color: disableBorderColor ?? commonTextFieldStyle.disableBorderColor!,
                    width: 1.0),
              ),
        focusedBorder: isUnderline
            ? UnderlineInputBorder(
                borderSide: BorderSide(
                    color: focusedBorderColor ?? commonTextFieldStyle.focusedBorderColor!,
                    width: 1.0),
              )
            : OutlineInputBorder(
                borderRadius: BorderRadius.circular(radius ?? commonTextFieldStyle.borderRadius),
                borderSide: BorderSide(
                    color: focusedBorderColor ?? commonTextFieldStyle.focusedBorderColor!,
                    width: 1.0),
              ),
        enabledBorder: isUnderline
            ? UnderlineInputBorder(
                borderSide: BorderSide(
                    color: enableBorderColor ?? commonTextFieldStyle.enableBorderColor!,
                    width: 1.0),
              )
            : OutlineInputBorder(
                borderRadius: BorderRadius.circular(radius ?? commonTextFieldStyle.borderRadius),
                borderSide: BorderSide(
                    color: enableBorderColor ?? commonTextFieldStyle.enableBorderColor!,
                    width: 1.0),
              ),
        errorBorder: isUnderline
            ? UnderlineInputBorder(
                borderSide: BorderSide(
                    color: errorBorderColor ?? commonTextFieldStyle.errorBorderColor!,
                    width: 1.0),
              )
            : OutlineInputBorder(
                borderRadius: BorderRadius.circular(radius ?? commonTextFieldStyle.borderRadius),
                borderSide: BorderSide(
                    color: errorBorderColor ?? commonTextFieldStyle.errorBorderColor!,
                    width: 1.0),
              ),
        focusedErrorBorder: isUnderline
            ? UnderlineInputBorder(
                borderSide: BorderSide(
                    color: focusedErrorBorderColor ??
                        commonTextFieldStyle.focusedErrorBorderColor!,
                    width: 1.0),
              )
            : OutlineInputBorder(
                borderRadius: BorderRadius.circular(radius ?? commonTextFieldStyle.borderRadius),
                borderSide: BorderSide(
                    color: focusedErrorBorderColor ??
                        commonTextFieldStyle.focusedErrorBorderColor!,
                    width: 1.0),
              ),
      ),
    );
  }
}

class CommonTextFieldStyle {
  final TextStyle? titleStyle;
  final TextStyle? noteStyle;
  final TextStyle? labelStyle;
  final TextStyle? hintStyle;
  final TextStyle? errorStyle;
  final EdgeInsetsGeometry? contentPadding;
  final Color? borderColor;
  final Color? disableBorderColor;
  final Color? enableBorderColor;
  final Color? errorBorderColor;
  final Color? focusedBorderColor;
  final Color? focusedErrorBorderColor;
  final double borderRadius;
  final EdgeInsetsGeometry? titlePadding;

  const CommonTextFieldStyle({
    this.titleStyle =
        const TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.w600),
    this.noteStyle = const TextStyle(color: Colors.grey, fontSize: 16, fontWeight: FontWeight.w600),
    this.labelStyle =
        const TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.normal),
    this.hintStyle =
        const TextStyle(color: Colors.grey, fontSize: 16, fontWeight: FontWeight.normal),
    this.errorStyle =
        const TextStyle(color: Colors.redAccent, fontSize: 14, fontWeight: FontWeight.normal),
    this.contentPadding = const EdgeInsets.all(8.0),
    this.borderColor = Colors.grey,
    this.focusedErrorBorderColor = Colors.redAccent,
    this.disableBorderColor = const Color(0xFFE0E0E0),
    this.enableBorderColor = Colors.grey,
    this.errorBorderColor = Colors.redAccent,
    this.focusedBorderColor = Colors.blue,
    this.borderRadius = 8.0,
    this.titlePadding,
  });
}
