import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:unsplah_app/core/common/enum.dart';

class AppConfig {
  final Environment _environment;

  Map<String, dynamic> _config = {};

  String get baseUrl => _config[_Config.BASE_URL];

  String get accessKey => _config[_Config.ACCESS_KEY];

  Environment get env => _environment;

  AppConfig(Environment environment) : _environment = environment;

  Future<void> setup() async {
    await dotenv.load(fileName: '.env');
    switch (_environment) {
      case Environment.DEV:
        _config = _Config().devConstants;
        break;
      case Environment.PROD:
        _config = _Config().prodConstants;
        break;
    }
  }
}

class _Config {
  static const BASE_URL = 'BASE_URL';
  static const ACCESS_KEY = 'ACCESS_KEY';

  Map<String, dynamic> get devConstants => {
    BASE_URL: dotenv.env['BASE_URL_DEV'] ?? '',
    ACCESS_KEY: dotenv.env['ACCESS_KEY_DEV'] ?? '',
  };

  Map<String, dynamic> get prodConstants => {
    BASE_URL: dotenv.env['BASE_URL_PROD'] ?? '',
    ACCESS_KEY: dotenv.env['ACCESS_KEY_PROD'] ?? '',
  };
}
