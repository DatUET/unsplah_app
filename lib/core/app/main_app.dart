import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:unsplah_app/core/app/app.dart';
import 'package:unsplah_app/core/app/app_cubit/app_cubit.dart';
import 'package:unsplah_app/core/common/route.dart';
import 'package:unsplah_app/core/routing/route_observer.dart';
import 'package:unsplah_app/core/routing/routing.dart';
import 'package:unsplah_app/core/routing/sli_page.dart';
import 'package:unsplah_app/core/routing/sli_page_route.dart';
import 'package:unsplah_app/di/injection.dart';
import 'package:unsplah_app/l10n/l10n.dart';
import 'package:unsplah_app/widget/common_text_field.dart';

class MainApp extends StatelessWidget {
  const MainApp({Key? key}) : super(key: key);

  static SLIPageRoute<T> generator<T>(RouteSettings settings) {
    SLIPage page = AppPage.pages.firstWhere((element) => element.name == settings.name);
    return SLIPageRoute<T>(
      page: page.page,
      settings: settings,
      transitionDuration: page.transitionDuration ?? SLIRouting.defaultTransitionDuration,
      opaque: page.opaque,
      parameter: page.parameters,
      curve: page.curve,
      alignment: page.alignment,
      transition: page.transition,
      popGesture: page.popGesture,
      customTransition: page.customTransition,
      routeName: page.name,
      title: page.title,
      showCupertinoParallax: page.showCupertinoParallax,
      maintainState: page.maintainState,
      fullscreenDialog: page.fullscreenDialog,
      gestureWidth: page.gestureWidth,
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
          FocusManager.instance.primaryFocus?.unfocus();
        }
      },
      child: ScreenUtilInit(
        designSize: const Size(430, 932),
        minTextAdapt: true,
        splitScreenMode: true,
        useInheritedMediaQuery: true,
        builder: (context, child) {
          App.init();
          return BlocProvider<AppCubit>(
            create: (context) => Injector.getIt.get<AppCubit>()..getCurrentLang(),
            child: BlocBuilder<AppCubit, AppState>(
              builder: (context, state) {
                CommonTextField.commonTextFieldStyle = CommonTextFieldStyle(
                  titleStyle: App.appStyle?.medium12?.copyWith(
                    color: App.appColor?.grey,
                  ),
                  labelStyle: App.appStyle?.medium12?.copyWith(
                    color: App.appColor?.tertiary,
                  ),
                  hintStyle: App.appStyle?.medium12?.copyWith(
                    color: App.appColor?.grey,
                  ),
                  errorStyle: App.appStyle?.medium10?.copyWith(
                    color: App.appColor?.redBase,
                  ),
                  contentPadding: EdgeInsets.symmetric(vertical: 14.h, horizontal: 12.w),
                  borderColor: App.appColor?.borderColor,
                  enableBorderColor: App.appColor?.borderColor,
                  disableBorderColor: App.appColor?.borderColor,
                  focusedBorderColor: App.appColor?.secondary,
                  focusedErrorBorderColor: App.appColor?.redBase,
                  errorBorderColor: App.appColor?.redBase,
                );
                return MaterialApp(
                  builder: (context, widget) {
                    return MediaQuery(
                        data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                        child: widget!);
                  },
                  debugShowCheckedModeBanner: false,
                  locale: state.locale,
                  title: 'Unsplash',
                  theme: App.theme?.lightTheme,
                  darkTheme: App.theme?.darkTheme,
                  themeMode: ThemeMode.light,
                  initialRoute: AppPage.SPLASH,
                  onGenerateRoute: (settings) => generator(settings),
                  navigatorKey: SLIRouting.key,
                  navigatorObservers: [
                    SLIRouteObserver(SLIRouting.routing),
                  ],
                  localizationsDelegates: const [
                    AppLocalizations.delegate,
                    // ServerMessageLocalization.delegate,
                    GlobalMaterialLocalizations.delegate,
                    GlobalCupertinoLocalizations.delegate,
                  ],
                  supportedLocales: AppLocalizations.supportedLocales,
                );
              },
            ),
          );
        },
      ),
    );
  }
}
