import 'package:flutter/material.dart';
import 'package:unsplah_app/core/app/app.dart';

class AppTheme {
  ThemeData get lightTheme {
    return ThemeData.light().copyWith(
      appBarTheme: AppBarTheme(
        backgroundColor: App.appColor?.backgroundApp,
        elevation: 0.0,
        centerTitle: true,
        iconTheme: IconThemeData(
          color: App.appColor?.tertiary,
        ),
        titleTextStyle: App.appStyle?.semiBold20?.copyWith(
            color: App.appColor?.tertiary
        ),
      ),
      scaffoldBackgroundColor: App.appColor?.backgroundApp,
      primaryColor: App.appColor?.primaryColor,
      textSelectionTheme: TextSelectionThemeData(
          cursorColor: App.appColor?.secondary,
          selectionColor: App.appColor?.primaryColor?.withOpacity(0.56),
          selectionHandleColor: App.appColor?.primaryColor),
      chipTheme: ChipThemeData(
        selectedColor: App.appColor?.primaryColor,
        backgroundColor: Colors.white,
      ),
      dividerTheme: DividerThemeData(color: App.appColor?.grey),
    );
  }

  ThemeData get darkTheme {
    return ThemeData.dark().copyWith(
      appBarTheme: AppBarTheme(
        backgroundColor: App.appColor?.backgroundApp,
        elevation: 0.0,
        centerTitle: true,
        iconTheme: IconThemeData(
          color: App.appColor?.tertiary,
        ),
        titleTextStyle: App.appStyle?.semiBold20?.copyWith(
            color: App.appColor?.tertiary
        ),
      ),
      scaffoldBackgroundColor: App.appColor?.backgroundApp,
      primaryColor: App.appColor?.primaryColor,
      textSelectionTheme: TextSelectionThemeData(
          cursorColor: App.appColor?.secondary,
          selectionColor: App.appColor?.primaryColor?.withOpacity(0.56),
          selectionHandleColor: App.appColor?.primaryColor),
      chipTheme: ChipThemeData(
        selectedColor: App.appColor?.primaryColor,
        backgroundColor: Colors.white,
      ),
      dividerTheme: DividerThemeData(color: App.appColor?.grey),
    );
  }
}
