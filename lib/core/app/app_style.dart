import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppStyle {

  /// Font size = 10.sp
  TextStyle? medium10;
  TextStyle? semiBold10;
  TextStyle? bold10;

  /// Font size = 12.sp
  TextStyle? medium12;
  TextStyle? semiBold12;
  TextStyle? bold12;

  /// Font size = 16.sp
  TextStyle? medium16;
  TextStyle? semiBold16;
  TextStyle? bold16;

  /// Font size = 20.sp
  TextStyle? medium20;
  TextStyle? semiBold20;
  TextStyle? bold20;

  /// Font size = 24.sp
  TextStyle? medium24;
  TextStyle? semiBold24;
  TextStyle? bold24;

  /// Font size = 32.sp
  TextStyle? medium32;
  TextStyle? semiBold32;
  TextStyle? bold32;


  AppStyle() {
    medium10 = TextStyle(fontSize: 10.0.sp, fontWeight: FontWeight.w500);
    semiBold10 = TextStyle(fontSize: 10.0.sp, fontWeight: FontWeight.w600);
    bold10 = TextStyle(fontSize: 10.0.sp, fontWeight: FontWeight.bold);

    medium12 = TextStyle(fontSize: 12.0.sp, fontWeight: FontWeight.w500);
    semiBold12 = TextStyle(fontSize: 12.0.sp, fontWeight: FontWeight.w600);
    bold12 = TextStyle(fontSize: 12.0.sp, fontWeight: FontWeight.bold);

    medium16 = TextStyle(fontSize: 16.0.sp, fontWeight: FontWeight.w500);
    semiBold16 = TextStyle(fontSize: 16.0.sp, fontWeight: FontWeight.w600);
    bold16 = TextStyle(fontSize: 16.0.sp, fontWeight: FontWeight.bold);

    medium20 = TextStyle(fontSize: 20.0.sp, fontWeight: FontWeight.w500);
    semiBold20 = TextStyle(fontSize: 20.0.sp, fontWeight: FontWeight.w600);
    bold20 = TextStyle(fontSize: 20.0.sp, fontWeight: FontWeight.bold);

    medium24 = TextStyle(fontSize: 24.0.sp, fontWeight: FontWeight.w500);
    semiBold24 = TextStyle(fontSize: 24.0.sp, fontWeight: FontWeight.w600);
    bold24 = TextStyle(fontSize: 24.0.sp, fontWeight: FontWeight.bold);

    medium32 = TextStyle(fontSize: 32.0.sp, fontWeight: FontWeight.w500);
    semiBold32 = TextStyle(fontSize: 32.0.sp, fontWeight: FontWeight.w600);
    bold32 = TextStyle(fontSize: 32.0.sp, fontWeight: FontWeight.bold);
  }
}
