import 'package:flutter/material.dart';
import 'package:unsplah_app/core/routing/routing.dart';

class AppController {

  BuildContext? get context => SLIRouting.key.currentContext;

  bool get isDarkMode => (theme.brightness == Brightness.dark);

  Size? get screenSize {
    if (context != null) {
      return MediaQuery.of(context!).size;
    }
    return null;
  }

  ThemeData get theme {
    var theme = ThemeData.fallback();
    if (context != null) {
      theme = Theme.of(context!);
    }
    return theme;
  }
}
