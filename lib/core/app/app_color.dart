import 'package:flutter/material.dart';
import 'package:unsplah_app/core/app/app_controller.dart';
import 'package:unsplah_app/di/injection.dart';

class AppColor {
  Color? primaryColor;
  Color? white;
  Color? background;
  Color? backgroundApp;
  Color? grey;
  Color? tertiary;
  Color? secondary;
  Color? secondaryLight;
  Color? green;
  Color? black;
  Color? redBase;
  Color? borderColor;
  Color? filterOrderSelected;
  Color? filterOrderUnSelected;

  AppColor() {
    if (Injector.getIt.get<AppController>().isDarkMode) {
      setupDarkMode();
    } else {
      setupLightMode();
    }
  }

  void setupLightMode() {
    primaryColor = const Color(0xFF868BEF);
    white = const Color(0xFFFFFFFF);
    background = const Color(0xFFEFF5F9);
    backgroundApp = const Color(0xFFFFFFFF);
    grey = const Color(0xFFAAAAAA);
    tertiary = const Color(0xFF455662);
    secondary = const Color(0xFFC56E33);
    secondaryLight = const Color(0xFFFFD9B9);
    green = const Color(0xFF33C588);
    black = const Color(0xFF000000);
    redBase = const Color(0xFFFF5247);
    borderColor = const Color(0xFFCACFD3);
    filterOrderSelected = const Color(0xFFF7F0EC);
    filterOrderUnSelected = const Color(0xFFF6F4F6);
  }

  void setupDarkMode() {
    primaryColor = const Color(0xFF868BEF);
    white = const Color(0xFFFFFFFF);
    background = const Color(0xFFEFF5F9);
    backgroundApp = const Color(0xFFFFFFFF);
    grey = const Color(0xFFAAAAAA);
    tertiary = const Color(0xFF455662);
    secondary = const Color(0xFFC56E33);
    secondaryLight = const Color(0xFFFFD9B9);
    green = const Color(0xFF33C588);
    black = const Color(0xFF000000);
    redBase = const Color(0xFFFF5247);
    borderColor = const Color(0xFFCACFD3);
    filterOrderSelected = const Color(0xFFF7F0EC);
    filterOrderUnSelected = const Color(0xFFF6F4F6);
  }
}
