enum Environment { DEV, PROD }

enum LoadingStatus {
  initial,
  loading,
  refresh,
  loadMore,
  complete,
  noMoreData,
  loadMoreError,
  error,
}

enum AppLanguage {
  vi,
  en,
}
