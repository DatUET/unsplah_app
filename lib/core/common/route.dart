import 'package:unsplah_app/core/routing/sli_page.dart';
import 'package:unsplah_app/presentation/splash/view/splash_screen.dart';
import 'package:unsplah_app/presentation/list_photo/view/list_photo_screen.dart';
import 'package:unsplah_app/presentation/search_photo/view/search_photo_screen.dart';
import 'package:unsplah_app/presentation/detail_photo/view/detail_photo_screen.dart';

class AppPage {
  static const String SPLASH = '/';
  static const String LIST_PHOTO = '/list_photo';
  static const String SEARCH_PHOTO = '/search_photo';
  static const String DETAIL_PHOTO = '/detail_photo';

  static final List<SLIPage> pages = [
    SLIPage(name: SPLASH, page: splashScreenBuilder()),
    SLIPage(name: LIST_PHOTO, page: listPhotoScreenBuilder()),
    SLIPage(name: SEARCH_PHOTO, page: searchPhotoScreenBuilder()),
    SLIPage(name: DETAIL_PHOTO, page: detailPhotoScreenBuilder()),
  ];
}
