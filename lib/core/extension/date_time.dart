import 'dart:ui';

import 'package:intl/intl.dart';

final _dateFormatFull = DateFormat('dd/MM/yyyy HH:mm:ss');

extension DateTimeExtension on DateTime? {
  String get formatFull => (this != null) ? _dateFormatFull.format(this!) : '';
}
