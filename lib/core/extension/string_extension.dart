import 'dart:ui';

extension StringExtension on String? {
  bool get isNullOrEmpty => this == null || this!.trim().isEmpty;

  bool get isNotNullOrEmpty => this != null && this!.trim().isNotEmpty;

  DateTime? get timeFromMillisecond {
    if (isNullOrEmpty) return null;
    return DateTime.fromMillisecondsSinceEpoch(int.parse(this!));
  }

  bool get isUrl {
    if (isNullOrEmpty) return false;
    return this!.contains('http://') || this!.contains('https://');
  }

  Color? hexToColor({String alphaChannel = 'FF'}) {
    return this == null ? null : Color(int.parse(this!.replaceFirst('#', '0x$alphaChannel')));
  }
}