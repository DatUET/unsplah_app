import 'package:get_it/get_it.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:unsplah_app/core/app/app_config.dart';
import 'package:unsplah_app/core/app/app_controller.dart';
import 'package:unsplah_app/core/app/app_cubit/app_cubit.dart';
import 'package:unsplah_app/core/common/enum.dart';
import 'package:unsplah_app/data/datasource/local/app_local_data_source.dart';
import 'package:unsplah_app/data/datasource/remote/api_client.dart';
import 'package:unsplah_app/data/datasource/remote/photo_remote_data_source.dart';
import 'package:unsplah_app/data/repositories/app_repo_impl.dart';
import 'package:unsplah_app/data/repositories/photo_repo_impl.dart';
import 'package:unsplah_app/domain/repositories/app_repo.dart';
import 'package:unsplah_app/domain/repositories/photo_repo.dart';
import 'package:unsplah_app/domain/use_case/app_use_case.dart';
import 'package:unsplah_app/domain/use_case/photo_use_case.dart';
import 'package:unsplah_app/presentation/detail_photo/cubit/detail_photo_cubit.dart';
import 'package:unsplah_app/presentation/list_photo/cubit/list_photo_cubit.dart';
import 'package:unsplah_app/presentation/search_photo/cubit/search_photo_cubit.dart';
import 'package:unsplah_app/presentation/splash/cubit/splash_cubit.dart';

class Injector {
  static final getIt = GetIt.instance..allowReassignment = true;

  static Future<void> setupEnvironment(Environment env) async {
    AppConfig appConfig = AppConfig(env);
    await appConfig.setup();
    getIt.registerLazySingleton<AppConfig>(() => appConfig);
    getIt.registerFactory<AppController>(() => AppController());
  }

  static Future<void> setupData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    // di for local datasource
    getIt.registerLazySingleton<AppLocalDataSource>(() => AppLocalDataSource(sharedPreferences));
    // di for remote data source
    getIt
      ..registerLazySingleton<ApiHandler>(
          () => ApiClient(baseUrl: getIt.get<AppConfig>().baseUrl))
      ..registerLazySingleton<PhotoRemoteDataSource>(() => PhotoRemoteDataSourceImpl(getIt()));
  }

  static Future<void> setupDomain() async {
    // di for repositories
    getIt
      ..registerLazySingleton<AppRepo>(() => AppRepoImpl(getIt()))
      ..registerLazySingleton<PhotoRepo>(() => PhotoRepoImpl(getIt()));

    // di for usecase
    getIt
      ..registerLazySingleton<AppUseCase>(() => AppUseCase(getIt()))
      ..registerLazySingleton<PhotoUseCase>(() => PhotoUseCase(getIt()));
  }

  static Future<void> setupPresentation() async {
    getIt
      ..registerLazySingleton<AppCubit>(() => AppCubit())
      ..registerFactory<SplashCubit>(() => SplashCubit())
      ..registerFactory<ListPhotoCubit>(() => ListPhotoCubit())
      ..registerFactory<SearchPhotoCubit>(() => SearchPhotoCubit())
      ..registerFactory<DetailPhotoCubit>(() => DetailPhotoCubit());
  }
}
