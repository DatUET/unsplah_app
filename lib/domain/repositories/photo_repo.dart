import 'package:unsplah_app/domain/entities/list_response.dart';
import 'package:unsplah_app/domain/entities/photo.dart';

abstract class PhotoRepo {
  Future<List<Photo>> getListPhoto({int page = 1, int perPage = 10});

  Future<ListResponse<Photo>> searchPhoto(
      {int page = 1, int perPage = 10, required String keyword});

  Future<Photo> getPhotoDetail({required String id});
}
