abstract class PhotoLink {
  String? get self;
  String? get html;
  String? get download;
  String? get downloadLocation;
}