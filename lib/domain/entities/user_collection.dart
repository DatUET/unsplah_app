import 'package:unsplah_app/domain/entities/user.dart';

abstract class UserCollection {
  int? get id;
  String? get title;
  DateTime? get publishedAt;
  DateTime? get lastCollectedAt;
  DateTime? get updatedAt;
  String? get coverPhoto;
  User? get user;
}