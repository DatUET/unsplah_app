import 'package:unsplah_app/domain/entities/photo_link.dart';
import 'package:unsplah_app/domain/entities/photo_url.dart';
import 'package:unsplah_app/domain/entities/user.dart';
import 'package:unsplah_app/domain/entities/user_collection.dart';

abstract class Photo {
  String? get id;
  DateTime? get createdAt;
  DateTime? get updatedAt;
  int? get width;
  int? get height;
  String? get color;
  String? get blurHash;
  int? get likes;
  bool? get likedByUser;
  String? get description;
  User? get user;
  List<UserCollection>? get currentUserCollections;
  PhotoUrl? get urls;
  PhotoLink? get links;
}