abstract class UserLinks {
  String? get self;
  String? get html;
  String? get photos;
  String? get likes;
  String? get portfolio;
}