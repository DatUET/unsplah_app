abstract class ListResponse<T> {
  int? get total;
  int? get totalPages;
  List<T>? get data;
}