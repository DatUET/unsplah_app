abstract class PhotoUrl {
  String? get raw;
  String? get full;
  String? get regular;
  String? get small;
  String? get thumb;
}