import 'package:unsplah_app/domain/entities/profile_image.dart';
import 'package:unsplah_app/domain/entities/user_links.dart';

abstract class User {
  String? get id;
  String? get username;
  String? get name;
  String? get portfolioUrl;
  String? get bio;
  String? get location;
  int? get totalLikes;
  int? get totalPhotos;
  int? get totalCollections;
  String? get instagramUsername;
  String? get twitterUsername;
  ProfileImage? get profileImage;
  UserLinks? get links;
}