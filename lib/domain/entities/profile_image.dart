abstract class ProfileImage {
  String? get small;
  String? get medium;
  String? get large;
}