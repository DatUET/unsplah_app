import 'package:unsplah_app/domain/entities/list_response.dart';
import 'package:unsplah_app/domain/entities/photo.dart';
import 'package:unsplah_app/domain/repositories/photo_repo.dart';

class PhotoUseCase {
  final PhotoRepo _photoRepo;

  const PhotoUseCase(this._photoRepo);

  Future<List<Photo>> getListPhoto({int page = 1, int perPage = 10}) {
    return _photoRepo.getListPhoto(page: page, perPage: perPage);
  }

  Future<ListResponse<Photo>> searchPhoto(
      {int page = 1, int perPage = 10, required String keyword}) {
    return _photoRepo.searchPhoto(keyword: keyword, page: page, perPage: perPage);
  }

  Future<Photo> getPhotoDetail({required String id}) {
    return _photoRepo.getPhotoDetail(id: id);
  }
}
