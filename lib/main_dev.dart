import 'package:unsplah_app/bootstrap.dart';
import 'package:unsplah_app/core/app/main_app.dart';
import 'package:unsplah_app/core/common/enum.dart';

Future<void> main() async {
  bootstrap(() => const MainApp(), environment: Environment.DEV);
}