import 'package:json_annotation/json_annotation.dart';
import 'package:unsplah_app/domain/entities/user_links.dart';

part 'user_links_response_model.g.dart';

@JsonSerializable()
class UserLinksResponseModel implements UserLinks {
  UserLinksResponseModel({this.self, this.html, this.photos, this.likes, this.portfolio});

  @override
  @JsonKey(name: 'self')
  final String? self;
  
  @override
  @JsonKey(name: 'html')
  final String? html;
  
  @override
  @JsonKey(name: 'photos')
  final String? photos;
  
  @override
  @JsonKey(name: 'likes')
  final String? likes;
  
  @override
  @JsonKey(name: 'portfolio')
  final String? portfolio;

  factory UserLinksResponseModel.fromJson(Map<String, dynamic> json) =>
      _$UserLinksResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserLinksResponseModelToJson(this);
}