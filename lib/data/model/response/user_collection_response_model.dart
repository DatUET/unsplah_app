import 'package:json_annotation/json_annotation.dart';
import 'package:unsplah_app/core/adapter/time_iso8601_converter.dart';
import 'package:unsplah_app/data/model/response/user_response_model.dart';
import 'package:unsplah_app/domain/entities/user_collection.dart';

part 'user_collection_response_model.g.dart';

@JsonSerializable()
class UserCollectionResponseModel implements UserCollection {
  UserCollectionResponseModel({
    this.id,
    this.title,
    this.publishedAt,
    this.lastCollectedAt,
    this.updatedAt,
    this.coverPhoto,
    this.user,
  });

  @override
  @JsonKey(name: 'id')
  final int? id;

  @override
  @JsonKey(name: 'title')
  final String? title;

  @override
  @JsonKey(name: 'published_at')
  @TimeIso8601JsonConverter()
  final DateTime? publishedAt;

  @override
  @JsonKey(name: 'last_collected_at')
  @TimeIso8601JsonConverter()
  final DateTime? lastCollectedAt;

  @override
  @JsonKey(name: 'updated_at')
  @TimeIso8601JsonConverter()
  final DateTime? updatedAt;

  @override
  @JsonKey(name: 'cover_photo')
  final String? coverPhoto;

  @override
  @JsonKey(name: 'user')
  final UserResponseModel? user;

  factory UserCollectionResponseModel.fromJson(Map<String, dynamic> json) =>
      _$UserCollectionResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserCollectionResponseModelToJson(this);
}
