import 'package:json_annotation/json_annotation.dart';
import 'package:unsplah_app/domain/entities/profile_image.dart';

part 'profile_image_response_model.g.dart';

@JsonSerializable()
class ProfileImageResponseModel implements ProfileImage {
  ProfileImageResponseModel({this.small, this.medium, this.large});
  
  @override
  @JsonKey(name: 'small')
  final String? small;
  
  @override
  @JsonKey(name: 'medium')
  final String? medium;
  
  @override
  @JsonKey(name: 'large')
  final String? large;

  factory ProfileImageResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ProfileImageResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileImageResponseModelToJson(this);
}