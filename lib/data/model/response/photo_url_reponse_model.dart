import 'package:json_annotation/json_annotation.dart';
import 'package:unsplah_app/domain/entities/photo_url.dart';

part 'photo_url_reponse_model.g.dart';

@JsonSerializable()
class PhotoUrlResponseModel implements PhotoUrl {
  
  @override
  @JsonKey(name: 'raw')
  final String? raw;
  
  @override
  @JsonKey(name: 'full')
  final String? full;
  
  @override
  @JsonKey(name: 'regular')
  final String? regular;
  
  @override
  @JsonKey(name: 'small')
  final String? small;
  
  @override
  @JsonKey(name: 'thumb')
  final String? thumb;

  PhotoUrlResponseModel({this.raw, this.full, this.regular, this.small, this.thumb});

  factory PhotoUrlResponseModel.fromJson(Map<String, dynamic> json) =>
      _$PhotoUrlResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$PhotoUrlResponseModelToJson(this);
}