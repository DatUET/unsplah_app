// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_links_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserLinksResponseModel _$UserLinksResponseModelFromJson(
        Map<String, dynamic> json) =>
    UserLinksResponseModel(
      self: json['self'] as String?,
      html: json['html'] as String?,
      photos: json['photos'] as String?,
      likes: json['likes'] as String?,
      portfolio: json['portfolio'] as String?,
    );

Map<String, dynamic> _$UserLinksResponseModelToJson(
        UserLinksResponseModel instance) =>
    <String, dynamic>{
      'self': instance.self,
      'html': instance.html,
      'photos': instance.photos,
      'likes': instance.likes,
      'portfolio': instance.portfolio,
    };
