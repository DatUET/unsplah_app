// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_collection_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserCollectionResponseModel _$UserCollectionResponseModelFromJson(
        Map<String, dynamic> json) =>
    UserCollectionResponseModel(
      id: json['id'] as int?,
      title: json['title'] as String?,
      publishedAt: const TimeIso8601JsonConverter()
          .fromJson(json['published_at'] as String?),
      lastCollectedAt: const TimeIso8601JsonConverter()
          .fromJson(json['last_collected_at'] as String?),
      updatedAt: const TimeIso8601JsonConverter()
          .fromJson(json['updated_at'] as String?),
      coverPhoto: json['cover_photo'] as String?,
      user: json['user'] == null
          ? null
          : UserResponseModel.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserCollectionResponseModelToJson(
        UserCollectionResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'published_at':
          const TimeIso8601JsonConverter().toJson(instance.publishedAt),
      'last_collected_at':
          const TimeIso8601JsonConverter().toJson(instance.lastCollectedAt),
      'updated_at': const TimeIso8601JsonConverter().toJson(instance.updatedAt),
      'cover_photo': instance.coverPhoto,
      'user': instance.user,
    };
