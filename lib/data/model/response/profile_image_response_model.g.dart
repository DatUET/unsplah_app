// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_image_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileImageResponseModel _$ProfileImageResponseModelFromJson(
        Map<String, dynamic> json) =>
    ProfileImageResponseModel(
      small: json['small'] as String?,
      medium: json['medium'] as String?,
      large: json['large'] as String?,
    );

Map<String, dynamic> _$ProfileImageResponseModelToJson(
        ProfileImageResponseModel instance) =>
    <String, dynamic>{
      'small': instance.small,
      'medium': instance.medium,
      'large': instance.large,
    };
