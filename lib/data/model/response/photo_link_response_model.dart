import 'package:json_annotation/json_annotation.dart';
import 'package:unsplah_app/domain/entities/photo_link.dart';

part 'photo_link_response_model.g.dart';

@JsonSerializable()
class PhotoLinkResponseModel implements PhotoLink {
  PhotoLinkResponseModel({this.self, this.html, this.download, this.downloadLocation});
  
  @override
  @JsonKey(name: 'self')
  final String? self;
  
  @override
  @JsonKey(name: 'html')
  final String? html;
  
  @override
  @JsonKey(name: 'download')
  final String? download;
  
  @override
  @JsonKey(name: 'download_location')
  final String? downloadLocation;

  factory PhotoLinkResponseModel.fromJson(Map<String, dynamic> json) =>
      _$PhotoLinkResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$PhotoLinkResponseModelToJson(this);
}