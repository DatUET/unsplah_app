import 'package:json_annotation/json_annotation.dart';
import 'package:unsplah_app/data/model/response/profile_image_response_model.dart';
import 'package:unsplah_app/data/model/response/user_links_response_model.dart';
import 'package:unsplah_app/domain/entities/user.dart';

part 'user_response_model.g.dart';

@JsonSerializable()
class UserResponseModel implements User {
  UserResponseModel({
    this.id,
    this.username,
    this.name,
    this.portfolioUrl,
    this.bio,
    this.location,
    this.totalLikes,
    this.totalPhotos,
    this.totalCollections,
    this.instagramUsername,
    this.twitterUsername,
    this.profileImage,
    this.links,
  });

  @override
  @JsonKey(name: 'id')
  final String? id;

  @override
  @JsonKey(name: 'username')
  final String? username;

  @override
  @JsonKey(name: 'name')
  final String? name;

  @override
  @JsonKey(name: 'portfolio_url')
  final String? portfolioUrl;

  @override
  @JsonKey(name: 'bio')
  final String? bio;

  @override
  @JsonKey(name: 'location')
  final String? location;

  @override
  @JsonKey(name: 'total_likes')
  final int? totalLikes;

  @override
  @JsonKey(name: 'total_photos')
  final int? totalPhotos;

  @override
  @JsonKey(name: 'total_collections')
  final int? totalCollections;

  @override
  @JsonKey(name: 'instagram_username')
  final String? instagramUsername;

  @override
  @JsonKey(name: 'twitter_username')
  final String? twitterUsername;

  @override
  @JsonKey(name: 'profile_image')
  final ProfileImageResponseModel? profileImage;

  @override
  @JsonKey(name: 'links')
  final UserLinksResponseModel? links;

  factory UserResponseModel.fromJson(Map<String, dynamic> json) =>
      _$UserResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserResponseModelToJson(this);
}
