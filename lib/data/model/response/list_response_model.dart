import 'package:json_annotation/json_annotation.dart';
import 'package:unsplah_app/domain/entities/list_response.dart';

part 'list_response_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class ListResponseModel<T> implements ListResponse<T> {
  ListResponseModel({
    this.total,
    this.totalPages,
    this.data,
  });

  @override
  @JsonKey(name: 'total')
  final int? total;

  @override
  @JsonKey(name: 'total_pages')
  final int? totalPages;

  @override
  @JsonKey(name: 'results')
  final List<T>? data;

  factory ListResponseModel.fromJson(
          Map<String, dynamic> json, T Function(Object? json) fromJsonT) =>
      _$ListResponseModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object? Function(T value) toJsonT) =>
      _$ListResponseModelToJson(this, toJsonT);
}
