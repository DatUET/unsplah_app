import 'package:json_annotation/json_annotation.dart';
import 'package:unsplah_app/core/adapter/time_iso8601_converter.dart';
import 'package:unsplah_app/data/model/response/photo_link_response_model.dart';
import 'package:unsplah_app/data/model/response/photo_url_reponse_model.dart';
import 'package:unsplah_app/data/model/response/user_collection_response_model.dart';
import 'package:unsplah_app/data/model/response/user_response_model.dart';
import 'package:unsplah_app/domain/entities/photo.dart';

part 'photo_response_model.g.dart';

@JsonSerializable()
class PhotoResponseModel implements Photo {
  PhotoResponseModel({
    this.id,
    this.createdAt,
    this.updatedAt,
    this.width,
    this.height,
    this.color,
    this.blurHash,
    this.likes,
    this.likedByUser,
    this.description,
    this.user,
    this.currentUserCollections,
    this.urls,
    this.links,
  });

  @override
  @JsonKey(name: 'id')
  final String? id;

  @override
  @JsonKey(name: 'created_at')
  @TimeIso8601JsonConverter()
  final DateTime? createdAt;

  @override
  @JsonKey(name: 'updated_at')
  @TimeIso8601JsonConverter()
  final DateTime? updatedAt;

  @override
  @JsonKey(name: 'width')
  final int? width;

  @override
  @JsonKey(name: 'height')
  final int? height;

  @override
  @JsonKey(name: 'color')
  final String? color;

  @override
  @JsonKey(name: 'blur_hash')
  final String? blurHash;

  @override
  @JsonKey(name: 'likes')
  final int? likes;

  @override
  @JsonKey(name: 'liked_by_user')
  final bool? likedByUser;

  @override
  @JsonKey(name: 'description')
  final String? description;

  @override
  @JsonKey(name: 'user')
  final UserResponseModel? user;

  @override
  @JsonKey(name: 'current_user_collections')
  final List<UserCollectionResponseModel>? currentUserCollections;

  @override
  @JsonKey(name: 'urls')
  final PhotoUrlResponseModel? urls;

  @override
  @JsonKey(name: 'links')
  final PhotoLinkResponseModel? links;

  factory PhotoResponseModel.fromJson(Map<String, dynamic> json) =>
      _$PhotoResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$PhotoResponseModelToJson(this);
}
