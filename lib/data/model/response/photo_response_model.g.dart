// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'photo_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhotoResponseModel _$PhotoResponseModelFromJson(Map<String, dynamic> json) =>
    PhotoResponseModel(
      id: json['id'] as String?,
      createdAt: const TimeIso8601JsonConverter()
          .fromJson(json['created_at'] as String?),
      updatedAt: const TimeIso8601JsonConverter()
          .fromJson(json['updated_at'] as String?),
      width: json['width'] as int?,
      height: json['height'] as int?,
      color: json['color'] as String?,
      blurHash: json['blur_hash'] as String?,
      likes: json['likes'] as int?,
      likedByUser: json['liked_by_user'] as bool?,
      description: json['description'] as String?,
      user: json['user'] == null
          ? null
          : UserResponseModel.fromJson(json['user'] as Map<String, dynamic>),
      currentUserCollections: (json['current_user_collections']
              as List<dynamic>?)
          ?.map((e) =>
              UserCollectionResponseModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      urls: json['urls'] == null
          ? null
          : PhotoUrlResponseModel.fromJson(
              json['urls'] as Map<String, dynamic>),
      links: json['links'] == null
          ? null
          : PhotoLinkResponseModel.fromJson(
              json['links'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PhotoResponseModelToJson(PhotoResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'created_at': const TimeIso8601JsonConverter().toJson(instance.createdAt),
      'updated_at': const TimeIso8601JsonConverter().toJson(instance.updatedAt),
      'width': instance.width,
      'height': instance.height,
      'color': instance.color,
      'blur_hash': instance.blurHash,
      'likes': instance.likes,
      'liked_by_user': instance.likedByUser,
      'description': instance.description,
      'user': instance.user,
      'current_user_collections': instance.currentUserCollections,
      'urls': instance.urls,
      'links': instance.links,
    };
