// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'photo_link_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhotoLinkResponseModel _$PhotoLinkResponseModelFromJson(
        Map<String, dynamic> json) =>
    PhotoLinkResponseModel(
      self: json['self'] as String?,
      html: json['html'] as String?,
      download: json['download'] as String?,
      downloadLocation: json['download_location'] as String?,
    );

Map<String, dynamic> _$PhotoLinkResponseModelToJson(
        PhotoLinkResponseModel instance) =>
    <String, dynamic>{
      'self': instance.self,
      'html': instance.html,
      'download': instance.download,
      'download_location': instance.downloadLocation,
    };
