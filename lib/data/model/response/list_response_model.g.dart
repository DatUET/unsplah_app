// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListResponseModel<T> _$ListResponseModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    ListResponseModel<T>(
      total: json['total'] as int?,
      totalPages: json['total_pages'] as int?,
      data: (json['results'] as List<dynamic>?)?.map(fromJsonT).toList(),
    );

Map<String, dynamic> _$ListResponseModelToJson<T>(
  ListResponseModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'total': instance.total,
      'total_pages': instance.totalPages,
      'results': instance.data?.map(toJsonT).toList(),
    };
