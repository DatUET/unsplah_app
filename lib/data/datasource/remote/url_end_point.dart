class UrlEndPoint {
  static const _Photo photo = _Photo();
}

class _Photo {
  const _Photo();

  final String _photoPath = '/photos';
  final String _searchPath = '/search';

  String get photos => _photoPath;
  String get searchPhoto => '$_searchPath/photos';
}
