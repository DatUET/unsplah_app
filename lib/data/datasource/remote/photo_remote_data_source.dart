import 'package:unsplah_app/data/datasource/remote/api_client.dart';
import 'package:unsplah_app/data/datasource/remote/url_end_point.dart';
import 'package:unsplah_app/data/model/response/list_response_model.dart';
import 'package:unsplah_app/data/model/response/photo_response_model.dart';

abstract class PhotoRemoteDataSource {
  Future<List<PhotoResponseModel>> getListPhoto({int page = 1, int perPage = 10});

  Future<ListResponseModel<PhotoResponseModel>> searchPhoto(
      {int page = 1, int perPage = 10, required String keyword});

  Future<PhotoResponseModel> getPhotoDetail({required String id});
}

class PhotoRemoteDataSourceImpl implements PhotoRemoteDataSource {
  final ApiHandler _apiHandler;

  const PhotoRemoteDataSourceImpl(this._apiHandler);

  @override
  Future<List<PhotoResponseModel>> getListPhoto({int page = 1, int perPage = 10}) async {
    Map<String, dynamic> query = {};
    query['page'] = page;
    query['per_page'] = perPage;
    List<PhotoResponseModel> response = await _apiHandler.getList(
      UrlEndPoint.photo.photos,
      queryParameters: query,
      parser: (json) => PhotoResponseModel.fromJson(json),
    );
    return response;
  }

  @override
  Future<ListResponseModel<PhotoResponseModel>> searchPhoto(
      {int page = 1, int perPage = 10, required String keyword}) async {
    Map<String, dynamic> query = {};
    query['page'] = page;
    query['per_page'] = perPage;
    query['query'] = keyword;
    ListResponseModel<PhotoResponseModel> response = await _apiHandler.get(
      UrlEndPoint.photo.searchPhoto,
      queryParameters: query,
      parser: (json) => ListResponseModel.fromJson(
        json,
        (itemResult) => PhotoResponseModel.fromJson(itemResult as Map<String, dynamic>),
      ),
    );
    return response;
  }

  @override
  Future<PhotoResponseModel> getPhotoDetail({required String id}) async {
    PhotoResponseModel response = await _apiHandler.get(
      '${UrlEndPoint.photo.photos}/$id',
      parser: (json) => PhotoResponseModel.fromJson(json),
    );
    return response;
  }
}
