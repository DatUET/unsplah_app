import 'package:dio/dio.dart';

class AuthInterceptor extends QueuedInterceptor {
  final String accessKey;

  AuthInterceptor({required this.accessKey});

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.headers['Authorization'] = 'Client-ID $accessKey';
    return super.onRequest(options, handler);
  }
}
