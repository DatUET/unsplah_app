import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:unsplah_app/core/app/app_config.dart';
import 'package:unsplah_app/core/error/exception.dart';
import 'package:unsplah_app/data/datasource/remote/interceptor/auth_interceotor.dart';
import 'package:unsplah_app/data/datasource/remote/interceptor/curl_interceptor.dart';
import 'package:unsplah_app/di/injection.dart';

typedef ApiResponseToModelParser<T> = T Function(Map<String, dynamic> json);
typedef ReceiveProgress = Function(int, int);

abstract class ApiHandler {
  // parser JSON data {} => Object
  Future<T> post<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  });

  Future<List<T>> postList<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    dynamic body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  });

  Future<void> postWithoutParser<T>(
    String path, {
    dynamic body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  });

  // parser JSON data {} => Object
  Future<T> get<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? queryParameters,
    Options? options,
  });

  // parser JSON data [] => List Object
  Future<List<T>> getList<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? queryParameters,
    Options? options,
  });

  // parser JSON data {} => Object
  Future<T> put<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  });

  // parser JSON data {} => Object
  Future<T> patch<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  });

  // parser JSON data {} => Object
  Future<T> delete<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  });

  Future<void> download(
    String path,
    String savePath, {
    ReceiveProgress? receiveProgress,
    Map<String, dynamic>? data,
    Map<String, dynamic>? query,
  });
}

class ApiClient implements ApiHandler {
  Dio _dio = Dio();
  final String baseUrl;

  ApiClient({required this.baseUrl}) {
    init();
  }

  void init() {
    String accessKey = Injector.getIt.get<AppConfig>().accessKey;
    _dio = Dio(BaseOptions(
        followRedirects: false,
        baseUrl: baseUrl,
        connectTimeout: const Duration(seconds: 30),
        receiveTimeout: const Duration(seconds: 30),
        sendTimeout: const Duration(seconds: 30)));
    _dio.interceptors.addAll([
      AuthInterceptor(accessKey: accessKey),
      CurlInterceptor(),
      if (kDebugMode)
        LogInterceptor(
          request: true,
          requestBody: true,
          responseBody: true,
          responseHeader: true,
          requestHeader: true,
        ),
    ]);
  }

  @override
  Future<T> post<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) {
    return _remapError(() async {
      final response = await _dio.post(
        path,
        data: body,
        queryParameters: queryParameters,
        options: options,
      );
      return parser(response.data as Map<String, dynamic>);
    });
  }

  @override
  Future<List<T>> postList<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    dynamic body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) {
    return _remapError(() async {
      final response = await _dio.post(
        path,
        data: body,
        queryParameters: queryParameters,
        options: options,
      );
      return (response.data as List).map((e) => parser(e as Map<String, dynamic>)).toList();
    });
  }

  @override
  Future<void> postWithoutParser<T>(
    String path, {
    dynamic body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) async {
    await _dio.post(
      path,
      data: body,
      queryParameters: queryParameters,
      options: options,
    );
  }

  @override
  Future<T> get<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) {
    return _remapError(() async {
      final response = await _dio.get(
        path,
        queryParameters: queryParameters,
        options: options,
      );
      return parser(response.data as Map<String, dynamic>);
    });
  }

  @override
  Future<T> delete<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) {
    return _remapError(() async {
      final response = await _dio.delete(
        path,
        data: body,
        queryParameters: queryParameters,
        options: options,
      );
      return parser(response.data as Map<String, dynamic>);
    });
  }

  @override
  Future<T> put<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) {
    return _remapError(() async {
      final response = await _dio.put(
        path,
        data: body,
        queryParameters: queryParameters,
        options: options,
      );
      return parser(response.data as Map<String, dynamic>);
    });
  }

  @override
  Future<T> patch<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) {
    return _remapError(() async {
      final response = await _dio.patch(
        path,
        data: body,
        queryParameters: queryParameters,
        options: options,
      );
      return parser(response.data as Map<String, dynamic>);
    });
  }

  @override
  Future<List<T>> getList<T>(
    String path, {
    required ApiResponseToModelParser<T> parser,
    Map<String, dynamic>? queryParameters,
    Options? options,
  }) {
    return _remapError(() async {
      final response = await _dio.get(
        path,
        queryParameters: queryParameters,
        options: options,
      );
      return (response.data as List).map((e) => parser(e as Map<String, dynamic>)).toList();
    });
  }

  @override
  Future<void> download(
    String path,
    String savePath, {
    ReceiveProgress? receiveProgress,
    Map<String, dynamic>? data,
    Map<String, dynamic>? query,
  }) async {
    await _dio.download(
      path,
      savePath,
      onReceiveProgress: receiveProgress,
      data: data,
      queryParameters: query,
    );
  }

  Future<T> _remapError<T>(ValueGetter<Future<T>> func) async {
    try {
      return await func();
    } catch (e) {
      throw await _apiErrorToInternalError(e);
    }
  }

  Future<dynamic> _apiErrorToInternalError(e) async {
    if (e is DioException) {
      if (e.type == DioExceptionType.connectionTimeout ||
          e.type == DioExceptionType.receiveTimeout ||
          (e.type == DioExceptionType.unknown && e.error is SocketException)) {
        return NetworkIssueException();
      }
      return ServerException(e);
    }
    return e;
  }
}
