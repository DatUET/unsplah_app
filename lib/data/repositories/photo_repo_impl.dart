import 'package:unsplah_app/data/datasource/remote/photo_remote_data_source.dart';
import 'package:unsplah_app/data/model/response/list_response_model.dart';
import 'package:unsplah_app/data/model/response/photo_response_model.dart';
import 'package:unsplah_app/domain/repositories/photo_repo.dart';

class PhotoRepoImpl implements PhotoRepo {
  final PhotoRemoteDataSource _photoRemoteDataSource;

  const PhotoRepoImpl(this._photoRemoteDataSource);

  @override
  Future<List<PhotoResponseModel>> getListPhoto({int page = 1, int perPage = 10}) {
    return _photoRemoteDataSource.getListPhoto(page: page, perPage: perPage);
  }

  @override
  Future<ListResponseModel<PhotoResponseModel>> searchPhoto(
      {int page = 1, int perPage = 10, required String keyword}) {
    return _photoRemoteDataSource.searchPhoto(keyword: keyword, perPage: perPage, page: page);
  }

  @override
  Future<PhotoResponseModel> getPhotoDetail({required String id}) {
    return _photoRemoteDataSource.getPhotoDetail(id: id);
  }
}
