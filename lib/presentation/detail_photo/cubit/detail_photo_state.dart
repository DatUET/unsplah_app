part of 'detail_photo_cubit.dart';

class DetailPhotoState extends BaseAppState with EquatableMixin {
  Photo? photo;

  DetailPhotoState({
    required LoadingStatus loading,
    dynamic error,
    this.photo,
  }) : super(loading: loading, error: error);

  factory DetailPhotoState.initial() {
    return DetailPhotoState(
      loading: LoadingStatus.initial,
      error: null,
    );
  }

  DetailPhotoState copyWith({LoadingStatus? loading, dynamic error, Photo? photo}) {
    return DetailPhotoState(
      loading: loading ?? this.loading,
      error: error,
      photo: photo ?? this.photo,
    );
  }

  @override
  List<Object?> get props => [loading, error, photo];
}

