import 'package:equatable/equatable.dart';
import 'package:unsplah_app/core/base_component/base_app_state.dart';
import 'package:unsplah_app/core/base_component/base_cubit.dart';
import 'package:unsplah_app/core/common/enum.dart';
import 'package:unsplah_app/core/routing/routing.dart';
import 'package:unsplah_app/di/injection.dart';
import 'package:unsplah_app/domain/entities/photo.dart';
import 'package:unsplah_app/domain/use_case/photo_use_case.dart';
import 'package:unsplah_app/presentation/global_handler.dart';

part 'detail_photo_state.dart';

class DetailPhotoCubit extends BaseCubit<DetailPhotoState> {
  String photoId = '';
  final PhotoUseCase _photoUseCase = Injector.getIt.get<PhotoUseCase>();

  DetailPhotoCubit() : super(DetailPhotoState.initial()) {
    dynamic data = SLIRouting.routing.args;
    if (data is Map<String, dynamic> && data.containsKey('photo_id')) {
      photoId = data['photo_id'];
    }
  }

  @override
  Future<void> load() async {
    _getCurrentPhotoDetail();
  }

  Future<void> _getCurrentPhotoDetail() async {
    try {
      emit(state.copyWith(loading: LoadingStatus.loading));
      Photo photo = await _photoUseCase.getPhotoDetail(id: photoId);
      emit(state.copyWith(loading: LoadingStatus.complete, photo: photo));
    } catch (e) {
      handleErrorResponse(e);
    }
  }
}

