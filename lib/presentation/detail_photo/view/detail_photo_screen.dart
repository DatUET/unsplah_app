import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:unsplah_app/core/app/app.dart';
import 'package:unsplah_app/core/extension/date_time.dart';
import 'package:unsplah_app/core/extension/string_extension.dart';
import 'package:unsplah_app/core/mixin/after_layout.dart';
import 'package:unsplah_app/di/injection.dart';
import 'package:unsplah_app/generated/assets.gen.dart';
import 'package:unsplah_app/l10n/l10n.dart';
import 'package:unsplah_app/presentation/detail_photo/cubit/detail_photo_cubit.dart';
import 'package:unsplah_app/widget/image_loading.dart';
import 'package:unsplah_app/widget/loading_screen.dart';

Widget detailPhotoScreenBuilder() => BlocProvider<DetailPhotoCubit>(
      create: (_) => Injector.getIt.get<DetailPhotoCubit>(),
      child: const DetailPhotoScreen(),
    );

class DetailPhotoScreen extends StatefulWidget {
  const DetailPhotoScreen({Key? key}) : super(key: key);

  @override
  _DetailPhotoScreenState createState() => _DetailPhotoScreenState();
}

class _DetailPhotoScreenState extends State<DetailPhotoScreen> with AfterLayoutMixin {
  DetailPhotoCubit? _detailPhotoCubit;

  @override
  void initState() {
    super.initState();
    _detailPhotoCubit = context.read<DetailPhotoCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    _detailPhotoCubit?.load();
  }

  @override
  void dispose() {
    _detailPhotoCubit?.close();
    super.dispose();
  }

  Widget _buildAppBar() {
    return Stack(
      children: [
        BlocBuilder<DetailPhotoCubit, DetailPhotoState>(
          builder: (context, state) => AspectRatio(
            aspectRatio: 16.0 / 9.0,
            child: state.photo?.urls?.regular == null
                ? const ImageLoading()
                : CachedNetworkImage(
                    imageUrl: state.photo!.urls!.regular!,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => const ImageLoading(),
                    errorWidget: (context, url, err) => Assets.images.logo.svg(),
                  ),
          ),
        ),
        Positioned.fill(
          child: BlocBuilder<DetailPhotoCubit, DetailPhotoState>(
            builder: (context, state) => DecoratedBox(
              decoration: BoxDecoration(
                color: state.photo?.color.hexToColor(alphaChannel: '61'),
              ),
            ),
          ),
        ),
        Positioned(
          top: kToolbarHeight,
          left: 16.w,
          child: const BackButton(
            color: Colors.white,
          ),
        )
      ],
    );
  }

  Widget _buildInfo() {
    return BlocBuilder<DetailPhotoCubit, DetailPhotoState>(
      builder: (context, state) => state.photo == null
          ? const SizedBox()
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    SizedBox.square(
                      dimension: 40.r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20.r),
                        child: CachedNetworkImage(
                          imageUrl: state.photo?.user?.profileImage?.medium ?? '',
                          fit: BoxFit.cover,
                          placeholder: (context, url) => const ImageLoading(),
                          errorWidget: (context, state, err) => Assets.images.logo.svg(),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 12.w,
                    ),
                    Expanded(
                      child: Text(
                        state.photo?.user?.name ?? '',
                        style: App.appStyle?.bold24?.copyWith(
                          color: App.appColor?.tertiary,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16.h,
                ),
                Text(
                  state.photo?.description ?? '',
                  style: App.appStyle?.semiBold16?.copyWith(
                    color: App.appColor?.tertiary,
                  ),
                ),
                SizedBox(
                  height: 16.h,
                ),
                Text(
                  '${context.l10n.createAt}: ${state.photo?.createdAt?.formatFull}',
                  style: App.appStyle?.medium16?.copyWith(
                    color: App.appColor?.tertiary,
                  ),
                ),
                SizedBox(
                  height: 8.h,
                ),
                Text(
                  '${context.l10n.updateAt}: ${state.photo?.updatedAt.formatFull}',
                  style: App.appStyle?.medium16?.copyWith(
                    color: App.appColor?.tertiary,
                  ),
                ),
              ],
            ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return LoadingScreen<DetailPhotoCubit, DetailPhotoState>(
      builder: (context, state) => Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildAppBar(),
            Padding(
              padding: EdgeInsets.all(16.w),
              child: _buildInfo(),
            )
          ],
        ),
      ),
    );
  }
}
