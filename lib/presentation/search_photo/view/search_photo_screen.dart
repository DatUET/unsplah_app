import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:unsplah_app/core/common/enum.dart';
import 'package:unsplah_app/core/mixin/after_layout.dart';
import 'package:unsplah_app/di/injection.dart';
import 'package:unsplah_app/generated/assets.gen.dart';
import 'package:unsplah_app/l10n/l10n.dart';
import 'package:unsplah_app/presentation/search_photo/cubit/search_photo_cubit.dart';
import 'package:unsplah_app/widget/common_text_field.dart';
import 'package:unsplah_app/widget/image_loading.dart';
import 'package:unsplah_app/widget/loading_screen.dart';
import 'package:unsplah_app/widget/photo_widget.dart';

Widget searchPhotoScreenBuilder() => BlocProvider<SearchPhotoCubit>(
      create: (_) => Injector.getIt.get<SearchPhotoCubit>(),
      child: const SearchPhotoScreen(),
    );

class SearchPhotoScreen extends StatefulWidget {
  const SearchPhotoScreen({Key? key}) : super(key: key);

  @override
  _SearchPhotoScreenState createState() => _SearchPhotoScreenState();
}

class _SearchPhotoScreenState extends State<SearchPhotoScreen> with AfterLayoutMixin {
  SearchPhotoCubit? _searchPhotoCubit;
  final RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    _searchPhotoCubit = context.read<SearchPhotoCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    _searchPhotoCubit?.load();
  }

  @override
  void dispose() {
    _searchPhotoCubit?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoadingScreen<SearchPhotoCubit, SearchPhotoState>(
      listener: (context, state) {
        switch (state.loading) {
          case LoadingStatus.initial:
            break;
          case LoadingStatus.loading:
            break;
          case LoadingStatus.refresh:
            break;
          case LoadingStatus.loadMore:
            break;
          case LoadingStatus.complete:
            _refreshController.loadComplete();
            _refreshController.refreshCompleted();
            break;
          case LoadingStatus.noMoreData:
            _refreshController.loadComplete();
            _refreshController.loadNoData();
            break;
          case LoadingStatus.loadMoreError:
            break;
          case LoadingStatus.error:
            break;
        }
      },
      opacity: 0.5,
      builder: (context, state) => Scaffold(
        appBar: AppBar(
          title: CommonTextField(
            hint: context.l10n.search,
            prefix: const Icon(
              Icons.search,
              color: Colors.black26,
            ),
            prefixBoxConstrains: BoxConstraints.tightFor(width: 32.w, height: 32.h),
            onChange: (value) => _searchPhotoCubit?.onChangeSearch(value),
          ),
        ),
        body: BlocBuilder<SearchPhotoCubit, SearchPhotoState>(
          buildWhen: (oldState, currentState) => oldState.photos != currentState.photos,
          builder: (context, state) {
            return SmartRefresher(
              controller: _refreshController,
              onRefresh: () => _searchPhotoCubit?.load(),
              onLoading: () => _searchPhotoCubit?.loadMore(),
              enablePullUp: true,
              child: GridView.builder(
                padding: EdgeInsets.all(16.w),
                itemCount: state.photos.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 12.w,
                    crossAxisSpacing: 12.w,
                    childAspectRatio: 9.0 / 16.0),
                itemBuilder: (context, index) => PhotoWidget(
                  photo: state.photos[index],
                  onTap: () => _searchPhotoCubit?.onTapPhoto(state.photos[index]),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
