import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:unsplah_app/core/base_component/base_app_state.dart';
import 'package:unsplah_app/core/base_component/base_cubit.dart';
import 'package:unsplah_app/core/common/constant.dart';
import 'package:unsplah_app/core/common/enum.dart';
import 'package:unsplah_app/core/common/route.dart';
import 'package:unsplah_app/core/routing/routing.dart';
import 'package:unsplah_app/di/injection.dart';
import 'package:unsplah_app/domain/entities/list_response.dart';
import 'package:unsplah_app/domain/entities/photo.dart';
import 'package:unsplah_app/domain/use_case/photo_use_case.dart';
import 'package:unsplah_app/presentation/global_handler.dart';

part 'search_photo_state.dart';

class SearchPhotoCubit extends BaseCubit<SearchPhotoState> {
  int _page = Pagination.firstPage;
  bool _isHasNextData = true;
  Timer? _timer;
  String _keyword = '';
  int _counter = -1;

  final PhotoUseCase _photoUseCase = Injector.getIt.get<PhotoUseCase>();

  SearchPhotoCubit() : super(SearchPhotoState.initial()) {
    _timer = Timer.periodic(const Duration(milliseconds: 600), (timer) {
      _counter--;
      if (_counter == 0) {
        _page = Pagination.firstPage;
        _searchImage();
      }
    });
  }

  @override
  Future<void> close() {
    _timer?.cancel();
    _timer = null;
    return super.close();
  }

  @override
  Future<void> load() async {
    _page = Pagination.firstPage;
    _searchImage();
  }

  Future<void> loadMore() async {
    if (_isHasNextData) {
      _page++;
      _searchImage();
    }
  }

  void onChangeSearch(String value) {
    _keyword = value;
    _counter = 2;
  }

  Future<void> _searchImage({int pageSize = Pagination.searchPageSize}) async {
    try {
      if (_page == Pagination.firstPage) {
        emit(state.copyWith(loading: LoadingStatus.loading, photos: []));
      }
      if (_keyword.isEmpty) {
        List<Photo> result = await _photoUseCase.getListPhoto(page: _page, perPage: pageSize);
        List<Photo> currentPhoto = [...state.photos];
        if (_page == Pagination.firstPage) {
          currentPhoto = result;
        } else {
          currentPhoto.addAll(result);
        }
        emit(state.copyWith(loading: LoadingStatus.complete, photos: currentPhoto));
      } else {
        ListResponse<Photo> result =
            await _photoUseCase.searchPhoto(page: _page, perPage: pageSize, keyword: _keyword);
        List<Photo> currentPhoto = [...state.photos];
        if (_page == Pagination.firstPage) {
          currentPhoto = result.data ?? [];
        } else {
          currentPhoto.addAll(result.data ?? []);
        }
        _isHasNextData = result.totalPages == null ? true : _page < result.totalPages!;
        emit(state.copyWith(
            loading: _isHasNextData ? LoadingStatus.complete : LoadingStatus.noMoreData,
            photos: currentPhoto));
      }
    } catch (e) {
      emit(state.copyWith(loading: LoadingStatus.complete));
      handleErrorResponse(e);
    }
  }

  void onTapPhoto(Photo photo) {
    SLIRouting.toNamed(
      AppPage.DETAIL_PHOTO,
      arguments: {
        'photo_id': photo.id,
      },
    );
  }
}
