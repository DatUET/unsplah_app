part of 'search_photo_cubit.dart';

class SearchPhotoState extends BaseAppState with EquatableMixin {
  List<Photo> photos;

  SearchPhotoState({
    required LoadingStatus loading,
    dynamic error,
    this.photos = const [],
  }) : super(loading: loading, error: error);

  factory SearchPhotoState.initial() {
    return SearchPhotoState(
      loading: LoadingStatus.initial,
      error: null,
    );
  }

  SearchPhotoState copyWith({LoadingStatus? loading, dynamic error, List<Photo>? photos}) {
    return SearchPhotoState(
      loading: loading ?? this.loading,
      error: error,
      photos: photos ?? this.photos,
    );
  }

  @override
  List<Object?> get props => [loading, error, photos];
}

