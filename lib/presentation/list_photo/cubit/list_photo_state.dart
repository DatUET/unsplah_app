part of 'list_photo_cubit.dart';

class ListPhotoState extends BaseAppState with EquatableMixin {
  List<Photo> photos;

  ListPhotoState({
    required LoadingStatus loading,
    dynamic error,
    this.photos = const [],
  }) : super(loading: loading, error: error);

  factory ListPhotoState.initial() {
    return ListPhotoState(
      loading: LoadingStatus.initial,
      error: null,
    );
  }

  ListPhotoState copyWith({LoadingStatus? loading, dynamic error, List<Photo>? photos}) {
    return ListPhotoState(
      loading: loading ?? this.loading,
      error: error,
      photos: photos ?? this.photos,
    );
  }

  @override
  List<Object?> get props => [loading, error, photos];
}
