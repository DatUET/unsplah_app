import 'package:equatable/equatable.dart';
import 'package:unsplah_app/core/base_component/base_app_state.dart';
import 'package:unsplah_app/core/base_component/base_cubit.dart';
import 'package:unsplah_app/core/common/constant.dart';
import 'package:unsplah_app/core/common/enum.dart';
import 'package:unsplah_app/core/common/route.dart';
import 'package:unsplah_app/core/routing/routing.dart';
import 'package:unsplah_app/di/injection.dart';
import 'package:unsplah_app/domain/entities/photo.dart';
import 'package:unsplah_app/domain/use_case/photo_use_case.dart';
import 'package:unsplah_app/presentation/global_handler.dart';

part 'list_photo_state.dart';

class ListPhotoCubit extends BaseCubit<ListPhotoState> {
  int _page = Pagination.firstPage;

  final PhotoUseCase _photoUseCase = Injector.getIt.get<PhotoUseCase>();

  ListPhotoCubit() : super(ListPhotoState.initial());

  @override
  Future<void> load() async {
    _page = Pagination.firstPage;
    _getListPhoto();
  }

  Future<void> loadMore() async {
    _page++;
    _getListPhoto();
  }

  Future<void> _getListPhoto({int pageSize = Pagination.photoListPageSize}) async {
    try {
      if (_page == Pagination.firstPage) {
        emit(state.copyWith(loading: LoadingStatus.loading));
      }
      List<Photo> result = await _photoUseCase.getListPhoto(page: _page, perPage: pageSize);
      List<Photo> currentPhoto = [...state.photos];
      if (_page == Pagination.firstPage) {
        currentPhoto = result;
      } else {
        currentPhoto.addAll(result);
      }
      emit(state.copyWith(loading: LoadingStatus.complete, photos: currentPhoto));
    } catch (e) {
      handleErrorResponse(e, onRetry: () => _getListPhoto());
    }
  }

  void onTapPhoto(Photo photo) {
    SLIRouting.toNamed(
      AppPage.DETAIL_PHOTO,
      arguments: {
        'photo_id': photo.id,
      },
    );
  }
}
