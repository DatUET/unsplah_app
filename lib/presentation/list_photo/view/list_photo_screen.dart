import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:unsplah_app/core/app/app_cubit/app_cubit.dart';
import 'package:unsplah_app/core/common/enum.dart';
import 'package:unsplah_app/core/common/route.dart';
import 'package:unsplah_app/core/mixin/after_layout.dart';
import 'package:unsplah_app/core/routing/routing.dart';
import 'package:unsplah_app/di/injection.dart';
import 'package:unsplah_app/generated/assets.gen.dart';
import 'package:unsplah_app/l10n/l10n.dart';
import 'package:unsplah_app/presentation/list_photo/cubit/list_photo_cubit.dart';
import 'package:unsplah_app/widget/dialog_util.dart';
import 'package:unsplah_app/widget/image_loading.dart';
import 'package:unsplah_app/widget/loading_screen.dart';
import 'package:unsplah_app/widget/photo_widget.dart';

Widget listPhotoScreenBuilder() => BlocProvider<ListPhotoCubit>(
      create: (_) => Injector.getIt.get<ListPhotoCubit>(),
      child: const ListPhotoScreen(),
    );

class ListPhotoScreen extends StatefulWidget {
  const ListPhotoScreen({Key? key}) : super(key: key);

  @override
  _ListPhotoScreenState createState() => _ListPhotoScreenState();
}

class _ListPhotoScreenState extends State<ListPhotoScreen> with AfterLayoutMixin {
  ListPhotoCubit? _listPhotoCubit;
  AppCubit? _appCubit;
  final RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    super.initState();
    _listPhotoCubit = context.read<ListPhotoCubit>();
    _appCubit = context.read<AppCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    _listPhotoCubit?.load();
  }

  @override
  void dispose() {
    _listPhotoCubit?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LoadingScreen<ListPhotoCubit, ListPhotoState>(
      listener: (context, state) {
        switch (state.loading) {
          case LoadingStatus.initial:
            break;
          case LoadingStatus.loading:
            break;
          case LoadingStatus.refresh:
            break;
          case LoadingStatus.loadMore:
            break;
          case LoadingStatus.complete:
            _refreshController.loadComplete();
            _refreshController.refreshCompleted();
            break;
          case LoadingStatus.noMoreData:
            _refreshController.loadNoData();
            break;
          case LoadingStatus.loadMoreError:
            break;
          case LoadingStatus.error:
            break;
        }
      },
      builder: (context, state) => Scaffold(
        appBar: AppBar(
          title: Text(context.l10n.home),
          actions: [
            IconButton(
              icon: const Icon(Icons.search),
              splashRadius: 20.r,
              onPressed: () => SLIRouting.toNamed(AppPage.SEARCH_PHOTO),
            ),
            IconButton(
              splashRadius: 20.r,
              icon: const Icon(Icons.language),
              onPressed: () => _appCubit?.changeLanguage(),
            ),
          ],
        ),
        body: BlocBuilder<ListPhotoCubit, ListPhotoState>(
          buildWhen: (oldState, currentState) => oldState.photos != currentState.photos,
          builder: (context, state) {
            return SmartRefresher(
              controller: _refreshController,
              onRefresh: () => _listPhotoCubit?.load(),
              onLoading: () => _listPhotoCubit?.loadMore(),
              enablePullUp: true,
              child: GridView.builder(
                padding: EdgeInsets.all(16.w),
                itemCount: state.photos.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    mainAxisSpacing: 12.w,
                    crossAxisSpacing: 12.w,
                    childAspectRatio: 9.0 / 16.0),
                itemBuilder: (context, index) => PhotoWidget(
                  photo: state.photos[index],
                  onTap: () => _listPhotoCubit?.onTapPhoto(state.photos[index]),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
