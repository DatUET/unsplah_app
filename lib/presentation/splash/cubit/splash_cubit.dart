import 'package:equatable/equatable.dart';
import 'package:unsplah_app/core/base_component/base_app_state.dart';
import 'package:unsplah_app/core/base_component/base_cubit.dart';
import 'package:unsplah_app/core/common/enum.dart';
import 'package:unsplah_app/core/common/route.dart';
import 'package:unsplah_app/core/routing/routing.dart';

part 'splash_state.dart';

class SplashCubit extends BaseCubit<SplashState> {
  SplashCubit() : super(SplashState.initial());

  @override
  Future<void> load() async {
    Future.delayed(const Duration(milliseconds: 1400), () {
      SLIRouting.offAllNamed(AppPage.LIST_PHOTO);
    });
  }
}

