part of 'splash_cubit.dart';

class SplashState extends BaseAppState with EquatableMixin {
  SplashState({
    required LoadingStatus loading,
    dynamic error,
  }) : super(loading: loading, error: error);

  factory SplashState.initial() {
    return SplashState(
      loading: LoadingStatus.initial,
      error: null,
    );
  }

  SplashState copyWith({LoadingStatus? loading, dynamic error}) {
    return SplashState(
      loading: loading ?? this.loading,
      error: error,
    );
  }

  @override
  List<Object?> get props => [loading, error];
}

