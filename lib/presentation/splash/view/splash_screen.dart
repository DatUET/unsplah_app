import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:unsplah_app/core/mixin/after_layout.dart';
import 'package:unsplah_app/di/injection.dart';
import 'package:unsplah_app/generated/assets.gen.dart';
import 'package:unsplah_app/presentation/splash/cubit/splash_cubit.dart';

Widget splashScreenBuilder() => BlocProvider<SplashCubit>(
      create: (_) => Injector.getIt.get<SplashCubit>(),
      child: const SplashScreen(),
    );

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with AfterLayoutMixin {
  SplashCubit? _splashCubit;

  @override
  void initState() {
    super.initState();
    _splashCubit = context.read<SplashCubit>();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    _splashCubit?.load();
  }

  @override
  void dispose() {
    _splashCubit?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Assets.images.logo.svg(),
      ),
    );
  }
}
